'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const product = mongoose.Schema({
  
   Biscuit: {type: String},
   Bread: {type: String},
   Buns: {type: String},
   Cakes: {type:String, required:true},
   Chocolate: {type: String},
   Cookies: {type:String},
   IceCream: {type: String},
   Pastry: {type:String},
   CreationDate: {type: Date},
   Status: {type: Boolean}

   
});

module.exports = mongoose.model('Product', product);