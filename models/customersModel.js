'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const customer = mongoose.Schema({
  
   Firstame:{type:String,required:true},
   Lastname:{type:String,required:true},
   Email: {type: String},
   StreetAddress1: {type: String},
   StreetAddress2: {type: String},
   City: {type: String},
   State: {type: String},
   Country: {type: String},
   CreationDate: {type: Date},

   ShipContactPerson : {type: String},
   ShipStreetAddress1: {type: String},
   ShipStreetAddress2: {type: String},
   ShipCity: {type: String},
   ShipState: {type: String},
   ShipCountry: {type: String},
   
   ContactEmail: {type: String},
   ContactNumber: {type: String},
   HowHearAboutus: {type: String},
   Feedback: {type: String},
   Suggestion: {type: String},
   Status: {type: Boolean}
  
 
});

module.exports = mongoose.model('Customer', customer);