'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const user = mongoose.Schema({
  
   firstname:{type:String,required:true},
   lastname:{type:String,required:true},
   username:{type:String},
   email: {type: String, required: true},
   password: {type: String, required: true},
   phonenumber: {type: String, required: true},
   status: {type:Boolean, required:true},
   role:{type:String},
   address: {type: String},
   
  
});

module.exports = mongoose.model('User', user);