'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const admin = mongoose.Schema({
  
   firstname:{type:String,required:true},
   lastname:{type:String,required:true},
   email: {type: String, required: true},
   password: {type: String, required: true},
   phonenumber: {type: String, required: true},
   role:{type:String},
   location: {type: String, required: true},
   status: {type:Boolean, required:true}
   
});

module.exports = mongoose.model('Admin', admin);



