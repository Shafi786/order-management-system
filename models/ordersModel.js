'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const order = mongoose.Schema({
  
  
   CreationDate: {type: Date},
   OrderNumber:{type:Number, required:true},
   Items: [],
   Quantity: [],
   Status: {type: Boolean}
 
    
});

module.exports = mongoose.model('Order', order);