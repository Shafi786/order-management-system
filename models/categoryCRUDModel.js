'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const category = mongoose.Schema({
  
   category_name:{type:String, required:true},
   status: {type:Boolean, required:true}
   
});

module.exports = mongoose.model('Category', category);
