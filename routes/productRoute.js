'use strict';
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Product = require('../models/productModel');



//Product Creation
router.post('/productcreation', function(req, res){
  
         const product = new Product({
           
            Biscuit: req.body.Biscuit,
            Bread: req.body.Bread,  
            Buns: req.body.Buns,
            Cakes: req.body.Cakes,
            Chocolate: req.body.Chocolate,
            Cookies: req.body.Cookies,  
            IceCream: req.body.IceCream,
            Pastry: req.body. Pastry,
            CreationDate: req.body.CreationDate,
            Status: req.body.Status
             
         });
        
         product.save().then(function(result) {
            console.log(result);
            res.status(200).json({
              success: 'New Product has been created'

            });
        }).catch(error => {
            res.status(500).json({
               error: err
            });
         });
      }
   );


 

 router.get('/get_all_user',function(req,res){
    Product.find({}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json(task);
     });
 
  });


  router.get('/read/:userId', function(req, res) {
    Product.findById(req.params.userId, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });


 router.put('/update/:userId', function(req, res) {
    Product.findOneAndUpdate({_id: req.params.userId}, req.body, {new: true}, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });



 router.delete('/delete/:userId', function(req,res){
    Product.deleteOne({_id: req.params.userId}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json({ message: 'Product Details successfully deleted' });
     });
 
  });

  

module.exports = router;