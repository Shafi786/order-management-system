'use strict';
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Order = require('../models/ordersModel');



//Order Creation
router.post('/ordercreation', function(req, res){
  
         const order = new Order({
           
            CreationDate: req.body.CreationDate,
            OrderNumber: req.body.OrderNumber,  
            Items: req.body.Items,
            Quantity: req.body.Quantity,
            Status: req.body.Status
            
               
         });
        
         order.save().then(function(result) {
            console.log(result);
            res.status(200).json({
              success: 'New Order has been created'

            });
        }).catch(error => {
            res.status(500).json({
               error: err
            });
         });
      }
   );


 

 router.get('/get_all_user',function(req,res){
    Order.find({}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json(task);
     });
 
  });


  router.get('/read/:userId', function(req, res) {
    Order.findById(req.params.userId, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });


 router.put('/update/:userId', function(req, res) {
    Order.findOneAndUpdate({_id: req.params.userId}, req.body, {new: true}, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });



 router.delete('/delete/:userId', function(req,res){
    Order.deleteOne({_id: req.params.userId}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json({ message: 'Order Details successfully deleted' });
     });
 
  });

  

module.exports = router;