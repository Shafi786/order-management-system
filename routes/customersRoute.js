'use strict';
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Customer = require('../models/customersModel');



//Customer Creation
router.post('/customercreation', function(req, res){
  
         const customer = new Customer({
           

            Firstame: req.body.Firstame,
            Lastname: req.body.Lastname,  
            Email: req.body.Email,
            StreetAddress1: req.body.StreetAddress1,
            StreetAddress2: req.body.StreetAddress2,  
            City: req.body.City,
            State: req.body. State,
            Country: req.body.Country,
            CreationDate: req.body.CreationDate,

            ShipContactPerson: req.body.ShipContactPerson,
            ShipStreetAddress1: req.body.ShipStreetAddress1,
            ShipStreetAddress2: req.body.ShipStreetAddress2,  
            ShipCity: req.body.ShipCity,
            ShipState: req.body. ShipState,
            ShipCountry: req.body.ShipCountry,  

            ContactEmail: req.body.ContactEmail,
            ContactNumber: req.body.ContactNumber,
            HowHearAboutus: req.body.HowHearAboutus,  
            Feedback: req.body.Feedback,
            Suggestion: req.body.Suggestion,
            Status: req.body.Status


                       
         });
        
         customer.save().then(function(result) {
            console.log(result);
            res.status(200).json({
              success: 'New Customer has been created'

            });
        }).catch(error => {
            res.status(500).json({
               error: err
            });
         });
      }
   );


 

 router.get('/get_all_user',function(req,res){
    Customer.find({}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json(task);
     });
 
  });


  router.get('/read/:userId', function(req, res) {
    Customer.findById(req.params.userId, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });


 router.put('/update/:userId', function(req, res) {
    Customer.findOneAndUpdate({_id: req.params.userId}, req.body, {new: true}, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });



 router.delete('/delete/:userId', function(req,res){
    Customer.deleteOne({_id: req.params.userId}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json({ message: 'Customer Details successfully deleted' });
     });
 
  });

  

module.exports = router;