'use strict';
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const User = require('../models/userloginModel');
const jwt = require('jsonwebtoken');



//User Creation
router.post('/register', function(req, res) {
   bcrypt.hash(req.body.password, 10, function(err, hash){
      if(err) {
         return res.status(500).json({
            error: err
         });
      }
      else {
         const user = new User({
           
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            username: req.body.username,
            email: req.body.email,
            password: hash,
            phonenumber: req.body.phonenumber,
            status:req.body.status,
            role: req.body.role,
            address: req.body.address
             
     


         });
        
         user.save().then(function(result) {   
            console.log(result);
            res.status(200).json({
               success: 'New user has been created',
               
               firstname: user.firstname,
               lastname: user.lastname,
               username: user.username,
               email: user.email,
               password: hash,
               phonenumber: user.phonenumber,
               status: user.status,
               role: user.role,
               address: user.address
               
           

            });
        }).catch(error => {
            res.status(500).json({
               error: err
            });
         });
      }
   });
});





router.post('/login', function(req, res){
   console.log(req.body)
   User.findOne({email: req.body.email})
      .exec()

    .then(function(user) {
       bcrypt.compare(req.body.password, user.password, function(err, result){
          console.log(result)
          if(err) {
             return res.status(401).json({
                failed: 'Unauthorized Access'
             });
          }
          
          
          if(result) {
           console.log(result)
            const JWTToken = jwt.sign({   
                 email: user.email,
                 _id: user._id
               },
               'secret',
                {
                  expiresIn: '2h'
                });
                return res.status(200).json({
                  success: 'Welcome to the JWT Auth',
                  firstname: user.firstname,
                  lastname: user.lastname,
                  username: user.username,
                   email: user.email,
                  token: JWTToken,
                  phonenumber: user.phonenumber,
                  status: user.status,
                   role: user.role,
                  address: user.address
              
              

                });
     
           }
          //
          if(result) {
             return res.status(200).json({
                success: 'Welcome to the JWT Auth'
             });
          }
          return res.status(401).json({
             failed: 'Unauthorized Access'
          });
       });
    })

    

    .catch(error => {
       res.status(500).json({
          error: error
       });
    });;
 });
 



 router.get('/get_all_user', function(req,res){
    User.find({}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json(task);
     });
 
  });


  router.get('/read/:userId', function(req, res) {
    User.findById(req.params.userId, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });


 router.put('/update/:userId', function(req, res) {
    User.findOneAndUpdate({_id: req.params.userId}, req.body, {new: true}, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });



 router.delete('/delete/:userId', function(req,res){
    User.deleteOne({_id: req.params.userId}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json({ message: 'User successfully deleted' });
     });
 
  });

  
module.exports = router;