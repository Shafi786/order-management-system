'use strict';
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Category = require('../models/categoryCRUDModel');



//CategoryCreation
router.post('/categorycreation', function(req, res){
  
         const category = new Category({
           
            category_name: req.body.category_name,
            status: req.body.status  
                       
         });
        
         category.save().then(function(result) {
            console.log(result);
            res.status(200).json({
              success: 'New Category has been created'

            });
        }).catch(error => {
            res.status(500).json({
               error: err
            });
         });
      }
   );


 
//Get all the Data 
 router.get('/get_all',function(req,res){
  Category.find({}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json(task);
     });
 
  });

//Get Particular Data
  router.get('/read/:Id', function(req, res) {
    Category.findById(req.params.Id, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });

//Update
 router.put('/update/:Id', function(req, res) {
  Category.findOneAndUpdate({_id: req.params.Id}, req.body, {new: true}, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });


//Delete
 router.delete('/delete/:Id', function(req,res){
  Category.deleteOne({_id: req.params.Id}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json({ message: 'Category Details successfully deleted' });
     });
 
  });

  

module.exports = router;