'use strict';
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Admin = require('../models/adminModel');
const jwt = require('jsonwebtoken');




router.post('/register', function(req, res) {
   bcrypt.hash(req.body.password, 10, function(err, hash){
      if(err) {
         return res.status(500).json({
            error: err
         });
      }
      else {
         const admin = new Admin({
           
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: hash,
            phonenumber: req.body.phonenumber,
            role: req.body.role,
            location: req.body.location,
            status:req.body.status 
         
         });
        
         admin.save().then(function(result) {   
            console.log(result);
            res.status(200).json({
               success: 'New user has been created',
               
               firstname: admin.firstname,
               lastname: admin.lastname,
                email: admin.email,
               password: hash,
                role: admin.role,
               phonenumber: admin.phonenumber,
               location: admin.location,
               status: admin.status 
           

            });
        }).catch(error => {
            res.status(500).json({
               error: err
            });
         });
      }
   });
});





router.post('/login', function(req, res){
   console.log(req.body)
   Admin.findOne({email: req.body.email})
      .exec()

    .then(function(admin) {
       bcrypt.compare(req.body.password, admin.password, function(err, result){
          console.log(result)
          if(err) {
             return res.status(401).json({
                failed: 'Unauthorized Access'
             });
          }
          
          
          if(result) {
           console.log(result)
            const JWTToken = jwt.sign({   
                 email: admin.email,
                 _id: admin._id
               },
               'secret',
                {
                  expiresIn: '2h'
                });
                return res.status(200).json({
                  success: 'Welcome to the JWT Auth',
                  firstname: admin.firstname,
               lastname: admin.lastname,
                   email: admin.email,
                  token: JWTToken,
                   role: admin.role,
               phonenumber:admin.phonenumber,
               location: admin.location,
               status:admin.status 
              

                });
     
           }
          //
          if(result) {
             return res.status(200).json({
                success: 'Welcome to the JWT Auth'
             });
          }
          return res.status(401).json({
             failed: 'Unauthorized Access'
          });
       });
    })

    

    .catch(error => {
       res.status(500).json({
          error: error
       });
    });;
 });
 
//

exports.findByProductName =  function(nam, callback) { 
   console.log("name"+nam);
   Product.find({ "name":  nam  }).exec(callback);
 };
 

router.get('/read/:Firstname', function(req, res) {
   Admin.findByFirstname(req.params.Firstname, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });


 router.get('/get_all_user', function(req,res){
   Admin.find({}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json(task);
     });
 
  });


  router.get('/read/:userId', function(req, res) {
   Admin.findById(req.params.userId, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });


 router.put('/update/:userId', function(req, res) {
   Admin.findOneAndUpdate({_id: req.params.userId}, req.body, {new: true}, function(err, task) {
     if (err)
       res.send(err);
     res.json(task);
   });
 });



 router.delete('/delete/:userId', function(req,res){
   Admin.remove({_id: req.params.userId}, function(err, task) {
     // console.log(task)
       if (err)
         res.send(err);
       res.json({ message: 'Task successfully deleted' });
     });
 
  });

  
module.exports = router;