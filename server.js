'use strict';
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  bodyParser = require('body-parser'),
  cors = require('cors'),
  admin = require('./routes/adminRoute'),
  user = require('./routes/userloginRoute'),
  customer = require('./routes/customersRoute'),
  product = require('./routes/productRoute'),
  order = require('./routes/ordersRoute'),
  category = require('./routes/categoryCRUDRoute');
  
  


  
//mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb+srv://shafi:root@cluster0-gwill.mongodb.net/test?retryWrites=true&w=majority', {useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false}); 


// mongoose instance connection url connection
// mongoose.Promise = global.Promise;
// mongoose.connect('mongodb://localhost/OMS', {useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false}); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());


app.listen(port);

console.log('Order-Management-System server started on: ' + port);



app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  if (req.method === 'OPTIONS') {
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
      res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH');
      res.header('Access-Control-Allow-Headers', 'Content-Type');
      res.header('Access-Control-Allow-Credentials', true);
      
      return res.status(200).json({});
  };
  next();
});


app.use(express.static(__dirname + '/static'));

// routes(app); 
app.use('/', admin);
app.use('/user', user);
app.use('/customer', customer);
app.use('/product', product);
app.use('/order', order);
app.use('/category', category);











